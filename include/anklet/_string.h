/*!
* \file  _string.h
* \brief 
*/
#pragma once

typedef const char* Str;

/*!
 *
 */
inline Str string_dup(Str value) {
#ifdef ANKLE_TARGET_PLATFORM_VCRT
    return _strdup( value );
#else
    return strdup( value );
#endif  /* def ANKLET_SERVER_TARGET_COMPILER_MSVC */
}

/*!
 *
 */
Str string_assign(Str left, Str right);

/*!
 *
 */
int string_index_of(Str self, Str match);

/*!
 *
 */
int string_compare_n(Str self, Str match, int n);

/*!
 *
 */
void dealloc_string(Str a_str);

#define     REFCOUNT_TYPE    int

#ifndef offset_of
#   define    offset_of(_Type_, _member_)    (ptrdiff_t)&(((_Type_ *)0)->_member_)
#endif  /* ndef offset_of */

#ifdef SS_STATISTICS
static struct ss_statistics {
    int32_t    calls;
    int32_t    hashed;
    int32_t    strcmps;
    int32_t    search;
    int32_t    linked;
} add_stats, add_ref_stats, free_stats, find_stats, hash_stats;

#    define GATHER(_n_)    (++ _n_)

#else
#    define GATHER(_n_)
#endif   /* def SS_STATISTICS */

#define TOPBIT    ((unsigned REFCOUNT_TYPE)1 << (sizeof(REFCOUNT_TYPE) * CHAR_BIT - 1))

#define PADDING   ((2 * sizeof(long) - sizeof(REFCOUNT_TYPE)) % sizeof(long)) + 1

typedef struct shared_string_s {
    union {
        struct shared_string_s    **array;
        struct shared_string_s     *previous;
    } u;

    struct shared_string_s         *next;

    REFCOUNT_TYPE                   ref_count;

    char                            _string[PADDING];
} SharedString;

#define SS(_x_)    (((SharedString *)_x_) - offset_of(SharedString, _string))

#define SS_DUMP_TABLE     1
#define SS_DUMP_TOTALS    2

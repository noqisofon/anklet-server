/*!
* \file  socket.h
* \brief
*/
#pragma once

enum socket_status {
    SOCK_STATUS_AVAILABLE,
    SOCK_STATUS_ADD,
    SOCK_STATUS_DEAD
};

typedef struct socket_list_s SocketList;

/*!
 * \struct socket_s
 *
 * 
 */
typedef struct socket_s {
    enum socket_status        status;
    int32_t                   fd;
    struct listen_info_s     *listen;
    struct server_map_s      *last_map;

    Str                       host;
} Socket;


void send_with_handling(Socket *a_socket, SocketList *sockets);

/*!
* \file
* \brief
*/
#pragma once

typedef    uint32_t    tag_t;

/*!
 *
 */
typedef struct game_object_s {
    struct player_s         *controller;

    struct game_object_s    *next;
    struct game_object_s    *prev;

    struct game_object_s    *inventry;

    struct game_object_s    *head;
    Map                     *map;

    Str                      artifact;
    Str                      name;

    Str                      anime_suffix;

    Str                      race_name;

    int16_t                  x, y;
    double                   speed;

    uint8_t                  type;
    uint8_t                  subtype;
    uint8_t                  client_type;
    uint16_t                 race_type;

    Str                      material_name;
    uint16_t                 material;

    uint8_t                  state;
    int32_t                  value;
    int16_t                  level;

    int32_t                  weight;
    int32_t                  weight_limit;

    int64_t                  permanent_exp;

    struct game_object_s    *owner;
    tag_t                    owner_count;

    uint32_t                 flags[4];
} GameObject;

typedef struct object_link_s {
    GameObject              *object;
    struct object_link_s    *next;
    tag_t                    id;
} GameObjectLink;

typedef struct object_link_iterator_s {
    GameObjectLink                   *link;
    int32_t                           value;
    struct object_link_iterator_s    *next;
} GameObjectLinkIterator;

extern GameObject   *the_all_objects;
extern GameObject   *the_active_objects;
extern GameObject   *the_free_objects;
extern GameObject    the_objs[START_MAX];

extern int32_t       the_allocated_objects;
extern int32_t       the_free_object_amount;

#define LOCK_OBJ(_that_)                                                \
    (!(_that_)->invisible && (_that_)->type != OT_PLAYER && (_that_)->type != OT_EVENT_CONNECTOR)

#define HEAD_OBJ(_that_)                                    \
    ((_that_)->head != NULL ? (_that_)->head : (_that_))

enum object_update_flags {
    UP_OBJ_INSERT = 1,
    UP_OBJ_REMOVE,
    UP_OBJ_CHANGE,
    UP_OBJ_FACE
};

enum object_free_flags {
    FREE_OBJ_INVENTRY            = 1,
    FREE_OBJ_NO_DESTORY_CALLBACK = 2,
    FREE_OBJ_DROP_ABOVE_FLOOR    = 4
};

typedef struct archtype_s {
    
} ArchType;

/*!
 * \brief バージョン情報を表示します。
 * 
 * \param self プレイヤーのゲームオブジェクト
 *
 */
void version(GameObject *self);

/*!
 * \brief プレイヤーがゲームをプレイし始める時に表示されるメッセージを表示します。
 * 
 * \param self プレイヤーのゲームオブジェクト
 *
 */
void start_info(GameObject *self);

/*!
* \file
* \brief
*/
#pragma once


/*!
 *
 */
void print_ext_message(Socket              *socket,
                       int                  color,
                       uint8_t              type,
                       uint8_t              subtype,
                       const char          *message);

/*!
 *
 */
void draw_ext_info(int32_t                  flags,
                   int32_t                  priority,
                   const GameObject        *a_player,
                   uint8_t                  type,
                   uint8_t                  subtype,
                   const char              *message);

/*!
 *
 */
void draw_ext_info_format(int32_t           flags,
                          int32_t           priority,
                          const GameObject *a_player,
                          uint8_t           type,
                          uint8_t           subtype,
                          const char       *format,
                          ...);

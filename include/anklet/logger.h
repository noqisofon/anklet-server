/*!
* \file
* \brief
*/
#pragma once

typedef enum log_level {
    LOG_LEVEL_ERROR = 0,
    LOG_LEVEL_INFO = 1,
    LOG_LEVEL_DEBUG = 2,
    LOG_LEVEL_MONSTER = 3,
} LogLevel;

/*!
 *
 */
void write_log( LogLevel log_level, const char *format, ... );

#define LOG_ERROR(_format_, ...)                            \
    write_log( LOG_LEVEL_ERROR, _format_, ## __VA_ARGS__ )

#define LOG_INFO(_format_, ...)                             \
    write_log( LOG_LEVEL_INFO , _format_, ## __VA_ARGS__ )

#define LOG_DEBUG(_format_, ...)                            \
    write_log( LOG_LEVEL_DEBUG , _format_, ## __VA_ARGS__ )

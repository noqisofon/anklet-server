/*!
* \file
* \brief
*/
#pragma once

enum map_layers {
    MAP_LAYER_FLOOR = 0,
    MAP_LAYER_NO_PICK1,
    MAP_LAYER_NO_PICK2,
    MAP_LAYER_ITEM1,
    MAP_LAYER_ITEM2,
    MAP_LAYER_ITEM3,
    MAP_LAYER_LIVING1,
    MAP_LAYER_LIVING2,
    MAP_LAYER_FLY1,
    MAP_LAYER_FLY2,

    MAP_LAYERS
};

enum map_contains {
    MAP_IN_MEMORY          = 1,
    MAP_SWAPPED,
    MAP_LOADING,
    MAP_SAVING
};

enum save_mode {
    SAVE_MODE_NORMAL   = 0,
    SAVE_MODE_INPLACE  = 1,
    SAVE_MODE_OVERLAY  = 2
};

/*!
 * \struct map_cell_s
 *
 * 
 */
typedef struct map_cell_s {
    uint16_t              map_chips[MAP_LAYERS];         /**< マップチップ番号です。 */
    int32_t               darkness;                      /**< このセルが闇に包まれているかどうかを表します。 */
} MapCell;

/*!
 * \def MAP_HEAD_OFFSET
 */
#define MAP_HEAD_OFFSET   8

/*!
 * \def MAX_CLIENT_X
 *
 */
#define MAX_CLIENT_X      (MAP_CLIENT_X + MAP_HEAD_OFFSET)
/*!
 * \def MAX_CLIENT_Y
 */
#define MAX_CLIENT_Y      (MAP_CLIENT_Y + MAP_HEAD_OFFSET)

/*!
 * \struct server_map_s
 *
 * 
 */
typedef struct server_map_s {
    struct map_cell_s     cells[MAX_CLIENT_X][MAX_CLIENT_Y];
} ServerMap;

ANKLET_API const char *const map_layer_name[MAP_LAYERS];


typedef struct map_s {
    struct map_s         *next;

    Str                   temporary_filename;
    Str                   name;

    int32_t               timeout;

    uint32_t              in_memory;
} Map;

/*!
* \file  top.h
* \brief 
*/
#pragma once

#include "target_conditionals.h"

#ifdef ANKLET_TARGET_COMPILER_MSVC
#   include "win32.h"
#else
#   include "config.h"
#endif  /* def ANKLET_TARGET_COMPILER_MSVC */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#ifdef HAVE_STRINGS_H
#   include <strings.h>
#endif  /* def HAVE_STRINGS_H */
#include <time.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>

#ifdef HAVE_INTTYPES_H
#   include <inttypes.h>
#endif  /* def HAVE_INTTYPES_H */

#ifdef HAVE_STDBOOL_H
#   include <stdbool.h>
#endif  /* def HAVE_STDBOOL_H */

#ifdef HAVE_SYS_SOCKET_H
#   include <sys/socket.h>
#endif  /* def HAVE_SYS_SOCKET_H */

#ifdef HAVE_UNISTD_H
#   include <unistd.h>
#endif  /* def def HAVE_UNISTD_H */

#ifdef ANKLET_TARGET_PLATFORM_MSYS
#   include <ws2tcpip.h>
#endif  /* def ANKLET_TARGET_PLATFORM_MSYS */

#ifndef HAVE__BOOL
typedef enum {
    false = 0,
    true = 1
} Bool;
#else
typedef     _Bool   Bool;
#endif  /* ndef HAVE__BOOL */

#ifdef HAVE_SIGNAL_H
#   include <signal.h>
#endif  /* def HAVE_SIGNAL_H */

#ifdef HAVE_DIRENT_H
#   include <dirent.h>
#endif  /* def HAVE_DIRENT_H */

#ifdef HAVE_DLFCN_H
#   include <dlfcn.h>
#endif  /* def HAVE_DLFCN_H */

#include "defines.h"
#include "_string.h"
#include "_integer.h"
#include "io_handle.h"
#include "io_path.h"
#include "logger.h"

#define MAP_CLIENT_X       25
#define MAP_CLIENT_Y       25

#define START_MAX         500

#define SOCKET_BUFSIZE    (256 * 1024)

#define MAX_ERRORS        256

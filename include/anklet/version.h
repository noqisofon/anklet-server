/*!
* \file
* \brief
*/
#pragma once


/*!
 * \def APPLICATION_NAME
 */
#define APPLICATION_NAME "Anklet"

/*!
 * \def FULL_VERSION
 */
#define FULL_VERSION      "0.0.1"

/*!
 * \def MAIL_ADDRESS
 */
#define MAIL_ADDRESS       "ned.rihine@gmail.com"

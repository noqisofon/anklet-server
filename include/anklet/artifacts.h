/*!
* \file  artifacts.h
* \brief
*/
#pragma once

typedef struct artifact_s {
    GameObject           *item;
    uint16_t              chance;
    uint8_t               difficulty;
    struct artifact_s    *next;
    LinkedString         *allowed;
    int32_t               allowed_size;
} Artifact;

typedef struct artifact_list_s {
    uint8_t                      type;
    uint16_t                     total_chance;

    struct artifact_list_s      *next;
    struct artifact_s           *items;
} ArtifactList;

/*!
 *
 */
void init_artifacts(void);

/*!
 *
 */
void init_archtype_pointers();

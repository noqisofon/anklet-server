/*!
 * \file  players.h
 * \brief プレイヤー
 */
#pragma once

/*!
 * \enum range_type
 */
typedef enum range_type {
    /*!
     * 
     */
    RANGE_BOTTOM          = -1,
    /*!
     * 
     */
    RANGE_NONE            =  0,
    /*!
     * 
     */
    RANGE_BOW,
    /*!
     * 
     */
    RANGE_MAGIC
} RangeType;

/*!
 * \struct client_spell_s
 */
typedef struct client_spell_s {
    struct client_spell_s    *next;

    GameObject               *spell;
    int16_t                   last_spell_cost;
    int16_t                   last_damage;
} ClientSpell;

/*!
 * \struct player_s
 */
typedef struct player_s {
    struct player_s    *next;

    Socket             *socket;
    char                map_level[MAX_BUFSIZE];

    uint8_t             state;

    uint32_t            hidden : 1;

    uint8_t             listening;

    GameObject         *entity;

    /*!
     * 誰が彼を殺したか？
     */
    char               killer;

    GameObject         *mark;
    GameObject         *transport;
} Player;

/*!
 * \brief
 *
 * \param a_player
 * \param flags
 *
 * \return  
 */
int save_player(GameObject *a_player, int32_t flags);

/*!
 * \brief 
 */
int verify_player(Str name, Str password);

/*!
 * \brief プレイヤーをログアウトさせます。
 *
 * \param [in] a_player    ログアウトするプレイヤー。
 * \param [in] draw_exit   ログアウトしたことを他のプレイヤーに表示するかどうか。   
 */
void leave(Player *a_player, int draw_exit);

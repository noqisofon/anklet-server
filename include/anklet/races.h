/*!
* \file races.h
* \brief
*/
#pragma once

typedef struct race_s {
    const char               *name;         /**< この種族の名前。 */
    int                       poplation;    /**< この種族に属している人の数。 */
    struct object_link_s    *members;

    struct race_s            *next;
} Race;


void init_races(void);

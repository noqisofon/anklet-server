/*!
* \file
* \brief
*/
#pragma once

#include "anklet/socket.h"
#include "anklet/socket_list.h"


/*!
 * \struct listen_info_s
 *
 * 
 */
struct listen_info_s {
    int                 family;
    int                 socket_type;
    int                 protocol;
    socklen_t           addr_len;
    struct sockaddr    *addr;
};

/*!
 * \struct buffer_s
 *
 * 
 */
typedef struct buffer_s {
    uint8_t             data[SOCKET_BUFSIZE];
    int32_t             start;
    int32_t             length;
} Buffer;

/*!
 * \def MAX_PASSWORD_FAILURES
 * パスワード試行回数です。
 *
 * デフォルトでは 5 回失敗するとキックされます。
 */
#define    MAX_PASSWORD_FAILURES    5


/*!
 * \brief anklet サーバーが作成したテンポラリファイルを全て削除します。
 */
void clean_tmp_files(void);

/*!
 * \brief 使用していたリソースをクリーンアップします。
 *
 * anklet サーバーが終了するときに呼び出されます。
 */
void cleanup(void);

/*!
 * 
 */
int forbid_play(void);

/*!
 * \brief anklet サーバーのメイン処理です。
 */
int server_main(int argc, char **argv);

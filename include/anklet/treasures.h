/*!
* \file  treasures.h
* \brief 
*/
#pragma once

/*!
 * \def CHANCE_FOR_ARTIFACT
 */
#define    CHANCE_FOR_ARTIFACT    20

enum generate_treasure_flag {
    GT_ENVIRONMENT              = 0x0001,
    GT_INVISIBLE                = 0x0002,
    GT_START_EQUIP              = 0x0004,
    GT_ONLY_GOOD                = 0x0008,
    GT_UPDATE_INV               = 0x0010,
    GT_MINIMAL                  = 0x0020
};



void dealloc_all_treasures(void);

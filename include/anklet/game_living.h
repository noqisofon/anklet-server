#pragma once
/*!
 * \file  game_living.h
 * \brief ゲーム内の生きとし生けるものを表す GameLiving を記述するヘッダー。
 */

#include <stdint.h>


/** Game living statistics */
enum living_status {
    STRENGTH = 0,              /**< Strength     腕力   */
    DEXTERITY,                 /**< Dexterity    器用さ */
    CONSTITUTION,              /**< Constitution 器用さ   */
    INTELLIGENCE,              /**< Intelligence 賢さ   */
    WISDOM,                    /**< Wisdom       判断   */
    CHARISMA,                  /**< Charisma     魅力   */

    NUM_STATUS
};

/*!
 * \def MAX_LEVEL
 * 
 */
#define MAX_LEVEL    130

typedef struct game_living_s {
    /*!
     * 腕力
     */
    int8_t          strength;
    /*!
     * 器用さ
     */
    int8_t          dexterity;
    /*!
     * 器用さ
     */
    int8_t          constitution;
    /*!
     * 賢さ
     */
    int8_t          intelligence;
    /*!
     * 判断
     */
    int8_t          wisdom;
    /*!
     * 魅力
     */
    int8_t          charisma;

    /*!
     * ウェポンクラス(技術力を示します)です。
     *
     * 低いほど良いです。WC と呼ばれるのかなぁ…。
     */
    int8_t          weapon_class;
    /*!
     * アーマークラス(回避率を示します)。
     * 
     * 低いほど良いです。
     * AC とも呼ばれます。
     */
    int8_t          armour_class;

    /*!
     * 運です。
     *
     * 時々あらゆる事に影響を及ぼします。
     */
    int8_t          luck;                

    /*!
     * 俗に言うヒットポイントです。
     *
     * これが 0 になると死んだり意識を失ったり、戦闘不能に陥ったりします。
     */
    int16_t         hit_point;
    /*!
     * 最大ヒットポイントです。
     * 
     * これ以上は回復しません。
     * レベルが上ったりすると、増えることがあります。
     */
    int16_t         max_hit_point;

    /*!
     * マナポイント。
     * 
     * MP とも呼ばれます。
     * このポイントを消費して呪文をキャスト(唱える)することができます。
     */
    int16_t         mana_point;
    /*!
     * 最大マナポイントです。
     * 
     * これ以上は回復しません。
     * レベルが上ったりすると、増えることがあります。
     */
    int16_t         max_mana_point;

    /*!
     * エンティティが今現在他のエンティティに与えることのできるおおよそのダメージです。
     */
    int16_t         damage;
} GameLiving;

/*!
 * \file  hiscore.h
 * \brief 
 */
#pragma once

void show_hiscore(GameObject *self, int32_t max, const char *match);

void hiscore_check(GameObject *self, int32_t flags);

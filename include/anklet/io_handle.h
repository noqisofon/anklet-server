/*!
 * \file io_handle.h
 */
#pragma once


/*!
 *
 */
FILE *file_open(Str filepath, Str mode);

/*!
 *
 */
Str file_get_line(Str line, size_t size, FILE *handle);

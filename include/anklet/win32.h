/*!
* \file
* \brief
*/
#pragma once

#pragma warning(disable: 4761)

#include <io.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <tchar.h>
#include <time.h>
#include <math.h>
#include <process.h>

#define     WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <winsock2.h>
#include <ws2tcpip.h>

/* #define __STDC__ 1 */

#define     HAVE_STDBOOL_H
#define     HAVE__BOOL

#define     HAVE_INTTYPES_H

#ifndef HAVE_SNPRINTF
#   define     HAVE_SNPRINTF    1
#endif  /* ndef HAVE_SNPRINTF */
#define     snprintf    _snprintf

#define     CS_LOGSTATS

#define     HAVE_SRAND
#ifndef HAVE_FCNTL_H
#   define     HAVE_FCNTL_H
#endif  /* ndef HAVE_FCNTL_H */
#ifndef HAVE_STDDEF_H
#   define     HAVE_STDDEF_H
#endif  /* ndef HAVE_STDDEF_H */

#define     GETTIMEOFDAY_TWO_ARGS
#define     MAXPATHLEN             256
#define     HAVE_STRTOL
#define     HAVE_STRERROR

/* Many defines to redirect unix functions fake standard unix values */
#define     inline                     __inline
#define     unlink(_a_)                _unlink( _a_ )
#define     mkdir(_a_)                 _mkdir( _a_ )
#define     getpid(_a_)                _getpid( _a_ )
#define     popen(_a_)                 _popen( _a_ )
#define     pclose(_a_)                _pclose( _a_ )
#define     vsnprintf                  _vsnprinf
#define     strtok_r(_x_, _y_, _z_)    strtok( _x_, _y_ )

#define     R_OK                       6    /* for __access() */
#define     F_OK                       6

#ifndef S_ISDIR
#   define     S_ISDIR(_x_)               (((_x_) & S_IFMT) == S_IFDIR)
#endif  /* ndef S_ISDIR */
#ifndef S_ISREG
#   define     S_ISREG(_x_)               (((_x_) & S_IFMT) == S_IFREG)
#endif  /* S_ISREG */

#define     DATADIR                    "share"
#define     LIBDIR                     "share"
#define     CONFDIR                    "share"

#define     sleep(_x_)                 Sleep( _x_ * 1000 )

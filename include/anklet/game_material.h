/*!
* \file
* \brief
*/
#pragma once

enum material_type {
    M_PAPER         =   1,    /**< 紙製 */
    M_IRON          =   2,    /**< 鉄製 */
    M_GLASS         =   4,    /**< ガラス製 */
    M_LEATHER       =   8,    /**< 皮製 */
    M_WOOD          =  16,    /**< 木製 */
    M_ORGANIC       =  32,    /**< 自然？製 */
    M_STONE         =  64,    /**< 石製 */
    M_CLOTH         = 128,    /**< 布製 */
    M_ADAMANT       = 256,    /**< アダマンタイト製 */
};

typedef struct material_type_s {
    const char                *name;           /**< 素材の名前 */
    const char                *description;    /**< 素材の説明 */
    enum material_type         type;           /**< 素材のタイプ */

    struct material_type_s    *next;
} MaterialType;


ANKLET_API MaterialType *the_material;

/*!
* \file
* \brief
*/
#pragma once

#define NROF_ATTACKS           26
#define NROF_ATTACK_MESSAGE    21
#define MAX_ATTACL_MESSAGE     21

enum attack_messages {
    ATM_ARROW = 0,
    ATM_DRAIN,
    ATM_ELEC,
    ATM_COLD
};

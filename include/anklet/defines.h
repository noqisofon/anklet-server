#pragma once
/*!
 * \file    defines.h
 * \brief   様々な定義を記述するヘッダー。
 */

typedef struct linked_string_s {
    const char                *name;
    struct linked_string_s    *next;
} LinkedString;

#define FABS(_x_)          ((_x_) < 0 ? -(_x_) : (_x_))

#ifndef MAX
#   define MAX(_x_, _y_)   ((_x_) < (_y_) ? (_x_) : (_y_))
#endif  /* ndef MAX */
#ifndef MIN
#   define MIN(_x_, _y_)   ((_x_) > (_y_) ? (_x_) : (_y_))
#endif  /* ndef MIN */

#define TYPED_NULL(_Type_)             ((_Type_ *)NULL)

#define NULL_P(_x_)                    ((_x_) == NULL)
#define TYPEDNULL_P(_Type_, _x_)       ((_x_) == ((_Type_ *)NULL))
#define ZERO_P(_x_)                    ((_x_) == 0)

#define LIST_GET_NEXT(_x_)             ((_x_)->next)
#define LIST_SET_NEXT(_a_, _b_)         (_a_)->next = _b_

#define DEALLOC(_ptr_)                 free( (void *)_ptr_ )

/*!
 * \def MAX_BUFSIZE
 *
 */
#define    MAX_BUFSIZE             256
/*!
 * \def VERY_BIG_BUFSIZE
 *
 */
#define    VERY_BIG_BUFSIZE       1024
/*!
 * \def HUGE_BUFSIZE
 *
 */
#define    HUGE_BUFSIZE           4096

/*!
 * \def MAX_ANIMATIONS
 *
 */
#define    MAX_ANIMATIONS          256

#ifndef MAX_NAME
/*!
 * \def MAX_NAME
 * 普通の名前用のバッファの長さです。
 */
#    define    MAX_NAME             48
#endif  /* ndef MAX_NAME */
/*!
 * \def BIG_NAME
 * ちょっと長めの名前用のバッファの長さです。
 */
#define    BIG_NAME                 32

/*!
 * \enum fatal_error
 * \relate fatal
 * 致命的なエラーコードを表します。
 */
enum fatal_error {
    /*!
     * 割り当てるメモリが無くなったことを意味します。
     */
    OUT_OF_MEMORY        = 0x8000e,
    /*!
     * 何らかのマップエラーを意味します。
     */
    MAP_ERROR            = 0x80013,
    /*!
     * あーきてーぶる？が小さすぎることを意味します。
     */
    ARCHTABLE_TOO_SMALL  = 0x80020,
    /*!
     * 何らかのシステムエラーが起こったことを意味します。
     */
    SEE_LAST_ERROR       = 0x80000
};

/*!
 * \enum object_type
 * GameObject の種類コードを表します。
 */
enum object_type {
    OT_PLAYER            = 1,
    OT_TRANSPORT,
    OT_ROD,
    OT_TREASURE,
    OT_FOOR,
    OT_POTION,
    OT_BOOK,
    OT_CLOCK,
    OT_ARROW,
    OT_BOW,
    OT_WEAPON,
    OT_ARMOUR,
    OT_ALTAR,
    OT_LOCKED_DOOR,
    OT_MAP,
    OT_DOOR,
    OT_KEY,
    OT_MOON_GATE,
    OT_TRIGGER,
    OT_DEAD_OBJECT,
    OT_FLOOR,
    OT_LIGHTER,

    MAX_OBJECT_TYPE
};

enum player_status {
    ST_PLAYING          = 0,
    ST_PLAY_AGAIN,
    ST_GET_NAME,
    ST_GET_PASSWORD,
    ST_CONFIRM_PASSWORD
};

/*!
 * \def QUERY_FLAG(_entity_, _p_)
 */
#define QUERY_FLAG(_entity_, _p_)                               \
    ( (_entity_)->flags[_p_ / 32] & ( 1U << ( _p_ % 32 ) ) )

enum themselves_flags {
    FLAG_ALIVE                 = 0,
    FLAG_WIZ,
    FLAG_REMOVED,
};

/*!
 * \def DEFAULT_LOG_FILE
 *
 */
#ifndef DEFAULT_LOG_FILE
#   ifdef WIN32
#       define    DEFAULT_LOG_FILE    "var\\anklet.log"
#   else
#       define    DEFAULT_LOG_FILE    "/var/log/anklet/logfile"
#   endif  /* def WIN32 */
#endif  /* ndef DEFAULT_LOG_FILE */

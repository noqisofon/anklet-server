/*!
* \file
* \brief
*/
#pragma once

typedef struct face_s {
    uint16_t       image_id;
    uint8_t        visibility;
    uint8_t        magic_map;
    uint8_t        smooth_face;
    const char    *name;
} Face;

typedef struct animation_s {
    uint16_t             id;
    const char          *name;
    uint8_t              animation_amount;
    uint8_t              facings;
    const Face         **faces;
} Animation;

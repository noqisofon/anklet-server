/*!
* \file
* \brief
*/
#pragma once

#include "anklet/client.h"

struct socket_list_s {
#ifdef CLIENT_TYPES_H
    size_t           length;
    unsigned char   *buf;
#else
    size_t           length;
    unsigned char    buf[MAX_SOCKBUF];
#endif  /* def CLIENT_TYPES_H */
};

/*!
 * \param sockets
 */
void socket_list_init(SocketList* sockets);

/*!
 * \param sockets
 */
void socket_list_reset(SocketList* sockets);

/*!
 * \param sockets
 * \param format
 */
void socket_list_add_printf(SocketList* sockets, const char *format, ...);

/*!
 * \param sockets
 */
void socket_list_terminate(SocketList* sockets);



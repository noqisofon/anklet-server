/*!
* \file  target_conditionals.h
* \brief どのようなプラットフォームでビルドされたかを表すプリプロセッサシンボルを宣言します。
*/
#pragma once


#ifdef _MSC_VER
/*!
 * \def ANKLET_TARGET_COMPILER_MSVC
 *
 */
#   define  ANKLET_TARGET_COMPILER_MSVC    1
/*!
 * \def ANKLE_TARGET_PLATFORM_VCRT
 *
 */
#   define ANKLE_TARGET_PLATFORM_VCRT      1
#endif  /* def MSC_VER */

#if defined(_WIN32) || defined(__WIN32__) || defined(_WINNT) || defined(_WIN64)
/*!
 * \def ANKLET_TARGET_OS_WINDOWS
 *
 */
#   define ANKLET_TARGET_OS_WINDOWS        1
#endif  /* defined(_WIN32) */

#if defined(__MINGW32__) || defined(__MINGW64__)
/*!
 * \def ANKLET_TARGET_PLATFORM_MSYS
 *
 */
#   define ANKLET_TARGET_PLATFORM_MSYS     1
#endif  /* defined(__MINGW32__) && defined(__MINGW64__) */

#if defined(__GNUC__)
/*!
 * \def ANKLET_TARGET_COMPLIER_GCC
 *
 */
#   define ANKLET_TARGET_COMPLIER_GCC      1
#endif  /* defined(__GNUC__) */

/*!
 * \def ANKLET_TARGET_OS_LINUX
 *
 */
#ifdef __LINUX__
#   define  ANKLET_TARGET_OS_LINUX         1
#endif  /* def __LINUX__ */

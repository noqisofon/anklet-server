/*!
* \file
* \brief
*/
#pragma once

/*!
 *
 */
inline int32_t parse_int( Str value ) {
#ifdef ANKLE_TARGET_PLATFORM_VCRT
    return _tstoi( value );
#else
    return  atoi( value );
#endif  /* def WIN32 */
}

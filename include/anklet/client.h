/*!
* \file
* \brief
*/
#pragma once


#define MAX_SOCKBUF    (2 + 65535 + 1)

enum new_draw_info {
    NDI_BLACK          = 0,
    NDI_WHITE,
    NDI_DARK_ORANGE,

    NDI_COLOR_MASK     = 0xff,

    NDI_UNIQUE         = 0x100,
    NDI_ALL            = 0x200,
    NDI_ALL_DMS        = 0x400
};

enum message_type {
    MSG_TYPE_BOOK           = 1,
    MSG_TYPE_ADMIN,
};

enum message_admin_type {
    MSG_TYPE_ADMIN_RULES    = 1,
    MSG_TYPE_ADMIN_NEWS,
    MSG_TYPE_ADMIN_PLAYER,
    MSG_TYPE_ADMIN_DM,
    MSG_TYPE_ADMIN_LOADSAVE,
    MSG_TYPE_ADMIN_LOGIN,
    MSG_TYPE_ADMIN_VERSION,
    MSG_TYPE_ADMIN_ERROR
};

/*!
* \file global.h
* \brief
*/
#pragma once

#include "top.h"

#ifndef ANKLET_API
#   define    ANKLET_API    extern
#endif  /* ndef ANKLET_API */

#include "login.h"

#include "game_material.h"
#include "game_living.h"
#include "game_map.h"
#include "game_object.h"

#include "server.h"
#include "players.h"

#include "arch.h"
#include "plugins.h"
#include "readable.h"
#include "recipe.h"
#include "godheads.h"
#include "region.h"
#include "faces.h"
#include "attack.h"
#include "spells.h"
#include "skills.h"
#include "monsters.h"
#include "treasures.h"
#include "animation.h"
#include "hiscore.h"
#include "quest.h"

#include "artifacts.h"
#include "races.h"

// ===========================================================================
//
// global variables
//
// ===========================================================================
ANKLET_API    Player      *the_first_player;
ANKLET_API    Map         *the_first_map;
ANKLET_API    Face        *the_first_faces;

ANKLET_API    Race        *the_first_race;

ANKLET_API    int32_t      the_trying_emergency_save;
ANKLET_API    uint32_t     the_error_count;

ANKLET_API    FILE        *the_log_file;
ANKLET_API    int32_t      the_reopen_log_file;
ANKLET_API    int32_t      the_exiting;

ANKLET_API    char        *the_first_map_path[MAX_BUFSIZE];

ANKLET_API    Animation   *the_animations;
ANKLET_API    int32_t      the_animations_amount;

ANKLET_API    int32_t      the_errors_amount;

// #define OBJECT_DECREASE_NROF_BY_ONE(__)

#define FREE_AND_CLEAR(_that_)                  \
    { free( _that_ ); _that_ = NULL; }

#ifndef ANKLET_SERVER_TARGET_OS_WINDOWS
#    if HAVE_DIRENT_H
#        include <dirent.h>
#    else
#    endif  /*  HAVE_DIRENT_H */
#else
#endif  /* ndef WIN32 */

typedef struct settings_s {
    const char    *log_filename;              /**< 使用するログファイルパス */
    uint16_t       cs_port;                   /**< Client/Server 用のポート番号です。 */

    LogLevel       default_debugging_mode;    /**<  */

    uint8_t        dump_flags;                /**<  */
    const char    *dump_arg;

    Bool           daemon_mode;               /**< もし真なら、だえもんもーどになります？ */

    const char    *conf_dir;                  /**<  */
    const char    *data_dir;
    const char    *map_dir;
    const char    *local_dir;
    const char    *player_dir;

    Bool           recycle_tmp_maps;

    int32_t        log_timestamp;
    char          *log_timestamp_format;

    LinkedString  *disabled_plugins;
} ANK_Settings;

ANKLET_API   ANK_Settings the_settings;

/*!
* \file
* \brief
*/
#include <stdarg.h>

#include "anklet/global.h"

int reopen_logfile = 0;

static const char *const loglevel_names[] = {
    "[E] ",
    "[I] ",
    "[D] ",
    "[M] "
};

#define putch(_buf_, _index_, _ch_)    _buf_[_index_] = _ch_
#define endch(_buf_, _index_)          putch(_buf_, _index_, '\0')

void write_log( LogLevel log_level, const char *format, ... ) {
    char buf[20480];

    char time_buf[2048];

    va_list args;
    va_start( args, format );

    endch( buf, 0 );
    if ( log_level <= the_settings.default_debugging_mode ) {
        endch( time_buf, 0 );

        if ( the_settings.log_timestamp ) {
            struct tm   *time_tmp;
            time_t       now = time( TYPED_NULL( time_t ) );

            time_tmp = localtime( &now );
            if ( time_tmp != NULL ) {
                if ( ZERO_P( strftime( time_buf, sizeof( time_buf ), the_settings.log_timestamp_format, time_tmp ) ) ) {
                    endch( time_buf, 0 );
                }
            }
        }

        vsnprintf( buf, sizeof( buf ), format, args );

#ifdef ANKLET_TARGET_OS_WINDOWS
        if ( time_buf[0] != 0 ) {
            fputs( time_buf, the_log_file );
            fputs( " ", the_log_file );
        }
        fputs( loglevel_names[log_level], the_log_file );
        fputs( buf, the_log_file );

#   if defined(DEBUG) || defined(_DEBUG)
        fflush( the_log_file );                               /* so flush this!! */
#   endif  /*  defined(DEBUG) || defined(_DEBUG) */

        if ( the_log_file != stderr ) {
            if ( time_buf[0] != 0 ) {
                fputs( time_buf, stderr );
                fputs( " ", stderr );
            }
            fputs( loglevel_names[log_level], stderr );
            fputs( buf, stderr );
        }
#else
        /* not ANKLET_TARGET_OS_WINDOWS */
        if ( reopen_logfile ) {
            reopen_logfile = 0;

            if ( fclose( the_log_file ) != 0 ) {
                /*
                 *
                 */
                perror( "tried to close log file after SIGHUP in logger.c:LOG()" );
            }

            if ( NULL_P( the_log_file = fopen( the_settings.log_filename, "a" ) ) ) {
                /*
                 *
                 */
                perror( "tried to open log file after SIGHUP in logger.c:LOG()" );
                emergency_save( 0 );
                clean_tmp_files();
                exit( 1 );
            }
            setvbuf( the_log_file, NULL, _IOLBF, 0 );
            LOG_INFO( "\"the_log_file\" reopended\n" );
        }

        if ( time_buf[0] != 0 ) {
            fputs( time_buf, the_log_file );
            fputs( " ", the_log_file );
        }
        fputs( loglevel_names[log_level], the_log_file );
        fputs( buf, the_log_file );

#endif  /* def ANKLET_TARGET_OS_WINDOWS */
    }

    if ( !the_exiting && !the_trying_emergency_save &&
        log_level == LOG_LEVEL_ERROR &&
        ++the_error_count > MAX_ERRORS ) {
        the_exiting = 1;

        if ( !the_trying_emergency_save ) {
            emergency_save( 0 );
        }
    }

    va_end( args );
}

/*!
* \file
* \brief
*/
#include "anklet/top.h"

void dealloc_string( Str a_str ) {
    SharedString *ss;

    GATHER( ss );

    ss = SS( a_str );

    if ( ( --ss->ref_count & ~TOPBIT ) == 0 ) {
        /* Remove this entry */
        if ( ss->ref_count & TOPBIT ) {
            /*
              We must put a new value into the hash_table[]
             */
            if ( LIST_GET_NEXT( ss ) ) {
                *( ss->u.array ) = LIST_GET_NEXT( ss );
                LIST_GET_NEXT( ss )->u.array = ss->u.array;
                LIST_GET_NEXT( ss )->ref_count |= TOPBIT;
            } else {
                *( ss->u.array ) = NULL;
            }
        } else {
            /* Relink and free this struct */
            if ( LIST_GET_NEXT( ss ) ) {
                LIST_GET_NEXT( ss )->u.previous = ss->u.previous;
            }
            LIST_SET_NEXT( ss->u.previous, LIST_GET_NEXT( ss ) );

            free( ss );
        }
    }
}

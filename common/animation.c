/*!
* \file
* \brief
*/
#include <stdio.h>
#include <assert.h>

#include "anklet/global.h"

void dump_animations( void ) {
    fprintf( stderr, "id    name                               faces\n" );

    for ( int i = 0; i < the_animations_amount; ++i ) {
        fprintf( stderr, "%5d %50s %5d\n",
            the_animations[i].id,
            the_animations[i].name,
            the_animations[i].animation_amount );
    }
}

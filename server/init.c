/*!
* \file
* \brief
*/
#include "anklet/global.h"
#include "anklet/loader.h"
#include "anklet/version.h"
/* #ifndef __CEXTRACT__ */
/* #   include <sproto.h> */
/* #endif  /\* ndef __CEXTRACT__ *\/ */

static void      init_before_play( void );
static void      init_startup( void );

static void      show_help( void );

static Race     *get_race_list( void );
static void      fatal_signal( int32_t make_core );

/*!
 *
 */
static void become_daemon( void ) {
    the_settings.daemon_mode = true;

    if ( the_settings.log_filename == NULL ) {
        the_settings.log_filename = DEFAULT_LOG_FILE;
    }
}

/*!
 *
 */
static void set_log_filepath( Str filename ) {
    the_settings.log_filename = filename;
}

/*!
 *
 */
static void set_default_debugging_mode( LogLevel log_level ) {
    the_settings.default_debugging_mode = log_level;
}

/*!
 *
 */
static void set_dump_flags( uint8_t dump_flag ) {
    the_settings.dump_flags = dump_flag;
}

/*!
 *
 */
static void set_dump_flags_and_args( uint8_t dump_flag, Str name ) {
    the_settings.dump_flags = dump_flag;
    the_settings.dump_arg = name;
}

/*!
 *
 */
static void set_data_dir( Str path ) {
    the_settings.data_dir = path;
}

/*!
 *
 */
static void set_conf_dir( Str path ) {
    the_settings.conf_dir = path;
}

/*!
 *
 */
static void set_map_dir( Str path ) {
    the_settings.map_dir = path;
}

/*!
 *
 */
static void set_local_dir( Str path ) {
    the_settings.local_dir = path;
}

/*!
 *
 */
static void set_cs_port( Str value ) {
    int32_t port = parse_int( value );

    if ( 0 >= port || port < 65535 ) {
        LOG_ERROR( "%d is an invalid c/s port number, must be between 1 and 65535\n", port );

        exit( 1 );
    }
    the_settings.cs_port = port;
}

/*!
 *
 */
static void disable_plugin( Str name ) {
    LinkedString  *disable = calloc( 1, sizeof( LinkedString ) );

    disable->next = the_settings.disabled_plugins;
    disable->name = string_dup( name );

    the_settings.disabled_plugins = disable;
}

static void dealloc_races( void );

static void dealloc_materials( void );

/*!
 *
 */
static void show_version( void ) {
    version( NULL );
    exit( 0 );
}

/*!
 *
 */
static void show_scores( void ) {
    show_hiscore( NULL, 9999, "" );
    exit( 0 );
}

/*!
 *
 */
static void show_score_with( Str data ) {
    show_hiscore( NULL, 9999, data );
    exit( 0 );
}

/*!
 *
 */
static void server_dump_animations( void ) {
    dump_animations();
    cleanup();
}

typedef void( *OPFunc0 )( void );
typedef void( *OPFunc1 )( const char *arg1 );
typedef void( *OPFunc2 )( const char *arg1, const char *arg2 );

struct command_line_option_s {
    const char *option;
    uint8_t     amount;
    uint8_t     pass;
    void( *proc )( );
};

static struct command_line_option_s options[] = {
    { "-h"            , 0, 1, show_help },
    { "--help"        , 0, 1, show_help },

    { "-v"            , 0, 1, show_version },
    { "--version"     , 0, 1, show_version },

    { "-d"            , 0, 1, set_default_debugging_mode },

    { "--show-scrores", 0, 1, show_scores }
};

static void show_help( void ) {
    printf( "Usage: anklet-server [options]\n\n" );

    printf( "Options:\n" );
    printf( "  --help, -h            Show this message.\n" );
    printf( "  --version, -v         Show version.\n" );

    exit( 0 );
}

static void init_before_play( void ) {
    init_archetypes();
    init_artifacts();

    check_spells();

    init_regions();
    init_archtype_pointers();
    init_races();
    init_godheads();
    init_readable();
    init_formulae();

    switch ( the_settings.dump_flags ) {
    case 1:
        print_monsters();
        cleanup();
    }
}

/*!
 *
 */
static void init_startup( void ) {
    char   buf[MAX_BUFSIZE];
    FILE  *fp;

#ifdef SHUTDOWN_FILE
    snprintf( buf, sizeof(buf), "%s/%s", settings.conf_dir, SHUTDOWN_FILE );

    if ( (fp = fopen( buf, "r" )) != NULL ) {
        while ( fgets( buf, MAX_BUFSIZE - 1, fp ) != NULL ) {
            printf( "%s", buf );
        }
        fclose( fp );

        exit( 1 );
    }
#endif  /* def SHUTDOWN_FILE */

    if ( forbid_play() ) {
        LOG_ERROR( "Anklet: Playing not allowed.\n" );

        exit( -1 );
    }
}

/*!
 *
 */
static void dealloc_materials( void ) {
    MaterialType *next;

    while ( the_material ) {
        next = LIST_GET_NEXT( the_material );
        DEALLOC( the_material );
        the_material = next;
    }
    the_material = NULL;
}

static void dealloc_races( void ) {
    Race              *a_race;
    GameObjectLink    *a_link;

    LOG_DEBUG( "Freeing race information\n" );
    while ( the_first_race ) {
        a_race = LIST_GET_NEXT( the_first_race );

        while ( the_first_race->members ) {
            a_link = LIST_GET_NEXT( the_first_race->members );
            DEALLOC( the_first_race->members );
            the_first_race->members = a_link;
        }
        dealloc_string( the_first_race->name );
        DEALLOC( the_first_race );
        the_first_race = a_race;
    }
}

void dealloc_server( void ) {
    dealloc_materials();
    dealloc_races();
    dealloc_quest();
    dealloc_quest_definitions();

    while ( the_settings.disabled_plugins ) {
        LinkedString *next = LIST_GET_NEXT( the_settings.disabled_plugins );

        DEALLOC( the_settings.disabled_plugins->name );
        DEALLOC( the_settings.disabled_plugins );

        the_settings.disabled_plugins = next;
    }
}

static Race *get_race(void) {
    Race    *a_list;

    a_list = (Race *)malloc( sizeof( Race ) );
    if ( !a_list ) {
        fatal( OUT_OF_MEMORY );
    }
    

    return a_list;
}

Race *find_race(Str name) {
    Race *test = NULL;

    if ( name && the_first_race ) {
        for ( test = the_first_race; test && test != LIST_GET_NEXT( test ); test = LIST_GET_NEXT( test ) ) {
            if ( !test->name || !strcmp( name, test->name ) ) {
                break;
            }
        }
    }

    return test;
}

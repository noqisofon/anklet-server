/*!
* \file
* \brief
*/
#include "anklet/global.h"
#include "anklet/socket_list.h"


void socket_list_init(SocketList* sockets) {
    socket_list_reset( sockets );
}

void socket_list_reset(SocketList* sockets) {
    sockets->length = 2;
}

void socket_list_add_printf(SocketList* sockets, const char *format, ...) {
    int      n;
    size_t   size;

    va_list args;
    {
        va_start( args, format );

        n = vsnprintf( (char *)sockets->buf + sockets->length, size, format, args );

        va_end( args );
    }

    if ( n <= -1 || (size_t)n >= size ) {
        fatal( OUT_OF_MEMORY );
    }
    sockets->length += (size_t)n;
}

void socket_list_terminate(SocketList* sockets) {
}

/*!
* \file
* \brief
*/
#include "anklet/global.h"

typedef struct quest_state_s {
    Str                      code;
    int32_t                  state;
    int32_t                  was_completed;
    int32_t                  is_complete;
    int32_t                  sent_to_client;

    struct quest_state_s    *next;
} QuestState;

typedef struct quest_player_s {
    Str                       player_name;
    struct quest_state_s     *quests;

    struct quest_player_s    *next;
} QuestPlayer;

static QuestPlayer    *player_states = NULL;

typedef struct quest_condition_s {
    Str                          quest_mode;
    int32_t                      min_step;
    int32_t                      max_step;

    struct quest_condition_s    *next;
} QuestCondition;

/*!
 *
 */
static void dealloc_quest_state( QuestPlayer *a_quest_player ) {
    QuestState    *a_qeuest_state = a_quest_player->quests, *next;

    while ( a_qeuest_state ) {
        next = LIST_GET_NEXT( a_qeuest_state );

        dealloc_string( a_qeuest_state->code );
        free( a_qeuest_state );

        a_qeuest_state = next;
    }
    a_quest_player->quests = NULL;
}

void dealloc_quest( void ) {
    QuestPlayer   *quest_player = player_states, *next_player;

    while ( quest_player ) {
        next_player = LIST_GET_NEXT( quest_player );

        dealloc_quest_state( quest_player );
        dealloc_string( quest_player->player_name );
        DEALLOC( quest_player );

        quest_player = next_player;
    }
}

/*!
 *
 */
void dealloc_quest_definitions( void ) {}

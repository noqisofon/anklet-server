/*!
* \file  console.c
* \brief
*/
#include "anklet/global.h"

#include "anklet/socket.h"
#include "anklet/socket_list.h"
#include "anklet/console.h"

void print_ext_message(Socket *socket, int color, uint8_t type, uint8_t subtype, const char *message) {
    SocketList sockets;

    socket_list_init( &sockets );
    socket_list_add_printf( &sockets, "draw_ext_info %d %hhu %s", color, type, subtype, message );
    send_with_handling( socket, &sockets );
    socket_list_terminate( &sockets );
}

void draw_ext_info(int32_t flags, int32_t priority, const GameObject* a_player, uint8_t type, uint8_t subtype, const char* message) {
    if ( flags & NDI_ALL || flags & NDI_ALL_DMS ) {
        for ( Player *it_player = the_first_player; it_player != NULL; it_player = it_player->next ) {
            if ( flags & NDI_ALL_DMS && !QUERY_FLAG( it_player->entity, FLAG_WIZ ) ) {

                continue;
            }

            draw_ext_info( flags & ~NDI_ALL & ~NDI_ALL_DMS, priority, it_player->entity, type, subtype, message );
        }

        return ;
    }

    if ( !a_player || ( a_player->type == OT_PLAYER && a_player->controller == NULL ) ) {

        return ;
    }
    if ( a_player->type != OT_PLAYER ) {

        return ;
    }
    if ( priority >= a_player->controller->listening ) {

        return ;
    }

    print_ext_message( a_player->controller->socket, flags & NDI_COLOR_MASK, type, subtype, message );
}

void draw_ext_info_format(int32_t flags, int32_t priority, const GameObject* a_player, uint8_t type, uint8_t subtype, const char* format, ...) {
    char message[HUGE_BUFSIZE];

    va_list args;
    {
        va_start( args, format );

        vsnprintf( message, HUGE_BUFSIZE, format, args );

        va_end( args );
    }

    draw_ext_info( flags, priority, a_player, type, subtype, message );
}

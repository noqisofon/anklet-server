/*!
* \file  server.c
* \brief
*/
#include <assert.h>

#include "anklet/global.h"
#include "anklet/console.h"
#include "anklet/version.h"


void version(GameObject *self) {
    draw_ext_info_format( NDI_UNIQUE, 0, self,
                          MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_VERSION,
                          "This is %s v%s",
                          APPLICATION_NAME,
                          FULL_VERSION );
    draw_ext_info_format( NDI_UNIQUE, 0, self,
                          MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_VERSION,
                          "The authors can be reached at %s",
                          MAIL_ADDRESS );
}

void start_info(GameObject *self) {
    draw_ext_info_format( NDI_UNIQUE, 0, self,
                          MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_LOGIN,
                          "Welcome to %s v%s!\nPress '?' for help\n",
                          APPLICATION_NAME,
                          VERSION );
    draw_ext_info_format( NDI_UNIQUE | NDI_ALL | NDI_DARK_ORANGE, 5, self,
                          MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_PLAYER,
                          "%s entered the game.",
                          self->name );
}

void clean_tmp_files(void) {
    Map *a_map, *next;

    LOG_INFO( "Cleaning up...\n" );

    for ( a_map = the_first_map; a_map != NULL; a_map = next ) {
        next = a_map->next;

        if ( a_map->in_memory == MAP_IN_MEMORY ) {

            if ( the_settings.recycle_tmp_maps ) {
                swap_map( a_map );
            } else {
                save_map( a_map, SAVE_MODE_NORMAL );
                clean_tmp_map( a_map );
            }
        }
    }

    write_tod_clock();
}

void cleanup(void) {
    LOG_DEBUG( "Cleanup called.  freeing data.\n" );

    clean_tmp_files();
    write_book_archive();
    accounts_save();

#ifdef MEMORY_DEBUG
    dealloc_all_maps();
    dealloc_style_maps();
#else
    cleanup_plugins();
#endif  /* def MEMORY_DEBUG */

    dealloc_all_archs();
    dealloc_all_treasures();

    exit( 0 );
}

void leave(Player *a_player, int draw_exit) {
    if ( !QUERY_FLAG( a_player->entity, FLAG_REMOVED ) ) {
        pets_terminate_all( a_player->entity );
        entity_remove( a_player->entity );
    }

    a_player->socket->status = SOCK_STATUS_DEAD;
    LOG_INFO( "LOGOUT: Player named %s from ip %s\n", a_player->entity->name, a_player->socket->host );

    string_assign( a_player->entity->controller->killer, "left" );
    hiscore_check( a_player->entity, 1 );

    GameObject *transport = a_player->transport;
    if ( transport && transport->controller == a_player ) {
        if ( transport->inventry ) {
            transport->controller = transport->inventry->controller;
        } else {
            transport->controller = NULL;
        }

        if ( transport->controller ) {
            char    name[MAX_BUFSIZE];

            query_name( transport, name, MAX_BUFSIZE );
            draw_ext_info_format( NDI_UNIQUE, 0, transport->controller->entity, MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_PLAYER,
                                  "%s has left.  You are now the captain of %s",
                                  a_player->entity->name, name);
        }
    }

    if ( a_player->entity->map ) {
        if ( a_player->entity->map->in_memory == MAP_IN_MEMORY ) {
            a_player->entity->map->timeout = MAP_TIMEOUT(a_player->entity->map);
        }
        a_player->entity->map = NULL;
    }
    a_player->entity->type = OT_DEAD_OBJECT;
    party_leave( a_player->entity );

    if ( !( QUERY_FLAG( a_player->entity, FLAG_WIZ ) && a_player->entity->controller->hidden ) &&
         ( a_player != NULL && draw_exit ) &&
         ( a_player->state != ST_GET_NAME && a_player->state != ST_GET_PASSWORD && a_player->state != ST_CONFIRM_PASSWORD ) ) {
        draw_ext_info_format( NDI_UNIQUE | NDI_ALL | NDI_DARK_ORANGE, 5, NULL,
                              MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_PLAYER,
                              "%s left in game.",
                              a_player->entity->name );
    }
}

int server_main(int argc, char **argv) {
#ifdef ANKLET_TARGET_COMPILER_MSVC
    Bool be_running = true;
#endif  /* def ANKLET_TARGET_COMPILER_MSVC */

    init( argc, argv );
    init_plugins();

#ifdef ANKLET_TARGET_COMPILER_MSVC
    while ( be_running ) {
#else
    for ( ;; ) {
#endif  /* def ANKLET_TARGET_COMPILER_MSVC */
        the_errors_amount = 0;

        do_server();
        process_events();
        process_anklet_time();

        execute_global_event( EVENT_CLOCK );

        check_active_maps();
        do_specials();

        sleep_delta();
    }

    return 0;
}

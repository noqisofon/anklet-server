/*!
 * \file  login.c
 * \brief ログイン用の関数の実装が含まれるソースファイル。
 */
#include "anklet/global.h"
#include "anklet/console.h"


void emergency_save(int32_t flags) {
#ifndef NO_EMERGENCY_SAVE
    Player    *a_player;

    the_trying_emergency_save = 1;
    LOG_ERROR( "Emergency save:  " );
    for ( a_player = the_first_player; a_player != NULL; a_player = LIST_GET_NEXT(a_player) ) {
        if ( !a_player->entity ) {
            LOG_ERROR( "No name, ignoring this\n" );

            continue;
        }
        LOG_ERROR( "%s ", a_player->entity->name );

        draw_ext_info( NDI_UNIQUE, 0, a_player->entity,
                       MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_LOADSAVE,
                       "Emergency save ..." );

        if ( !flags ) {
            string_assign( a_player->map_level, the_first_map_path );

            if ( a_player->entity->map != NULL ) {
                a_player->entity->map = NULL;
            }
            a_player->entity->x = -1;
            a_player->entity->y = -1;

            if ( save_player( a_player->entity, flags ) ) {
                LOG_ERROR( "(failed)" );

                draw_ext_info( NDI_UNIQUE, 0, a_player->entity,
                               MSG_TYPE_ADMIN, MSG_TYPE_ADMIN_LOADSAVE,
                               "Emergency save failed, checking score..." );
            }
            hiscore_check( a_player->entity, 1 );
        }
        LOG_ERROR( "\n" );
    }
#else
    LOG_INFO( "Emergency saves disabled, no save attempted\n" );
#endif  /* ndef NO_EMERGENCY_SAVE */
}

/*!
 *
 */
void delete_character(Str name) {
    char filepath[MAX_BUFSIZE];

    snprintf( filepath, sizeof( filepath ),
              "%s/%s/%s",
              the_settings.local_dir, the_settings.player_dir, name );

    remove_directory( filepath );
}

/*!
 * \enum verify_result_status
 * \relates verify_player
 * 
 * verify_player 関数の戻り値に使われます。
 */
enum verify_result_status {
    /*!
     * 成功
     */
    VERIFY_SUCCESS          =  0,
    /*!
     * 失敗
     */
    VERIFY_FAILED,
    /*!
     * 見つからなかった
     */
    VERIFY_NOT_FOUND
};

/*!
 * \def ARRAY_LAST_USELEN(_ary_, _get_len_fn_)
 * \relates verify_player
 *
 * 配列の末尾の要素を返します。
 * 配列の長さを返す関数を使って配列の長さを取得します。
 *
 * 横着がすぎるかもしれない。
 */
#define ARRAY_LAST_USELEN(_ary_, _get_len_fn_)    ARRAY_LAST( _ary_, _get_len_fn_( _ary_ ) )

/*!
 * \def ARRAY_LAST(_ary_, _length_)
 * \relates ARRAY_LAST_USELEN
 *
 * 配列の末尾の要素を返します。
 */
#define ARRAY_LAST(_ary_, _length_)               _ary_[(_length_) - 1]


int verify_player(Str name, Str password) {
    char     filepath[MAX_BUFSIZE];

    if ( string_index_of( name, "/.\\" ) != -1 ) {
        LOG_ERROR( "Username contains illegal characters: %s\n", name );

        return VERIFY_FAILED;
    }

    snprintf( filepath,
              sizeof(filepath),
              "%s/%s/%s/%s.pl",
              the_settings.local_dir,
              the_settings.player_dir,
              name,
              name );

    FILE    *fp = file_open( filepath, "r" );
    if ( NULL == fp ) {

        return VERIFY_FAILED;
    }

    char     line[MAX_BUFSIZE];
    while ( file_get_line( line, MAX_BUFSIZE - 1, fp ) != NULL ) {
        if ( !string_compare_n( line, "password ", 9 ) ) {
            ARRAY_LAST_USELEN( line, strlen ) = '\n';

            if ( check_password( password, line + 9 ) ) {
                fclose( fp );

                return VERIFY_SUCCESS;
            }
            fclose( fp );

            return VERIFY_NOT_FOUND;
        }
    }
    fclose( fp );

    LOG_DEBUG( "Could not find a password line in player: %s\n", name );

    return VERIFY_FAILED;
}
